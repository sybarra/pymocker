#!/bin/env python
from flask import json
from datetime import datetime
from random import randint, choice
from os import path
import csv
import string

def check_data_log_file(data_log, max_lines=50000):
    '''Return True if the exists and has less than the max_lines.
       Also if the data_log file does not exist created it.
    '''
    if path.exists(data_log):
        # Count the line in the file
        with open(data_log, "r") as tdf:
            for i, line in enumerate(tdf):
                if i < max_lines:
                    pass
                else:
                    return False
    else:
        # Create the data_log file
        with open(data_log, "w") as tdf:
            writer = csv.writer(tdf)
            writer.writerow(["Transaction Address"])
    return True


def add_to_data_log(log_file, data):
    '''
    param: data (dict)
    '''
    with open(log_file, "a") as f:
        writer = csv.writer(f)
        writer.writerow([data['Address'].split("#")[0]])


def list_from_csv(inputfile):
    with open(inputfile, "r") as csvfile:
        csv_reader = csv.reader(csvfile)
        rows = [row for row in csv_reader]
        rows.pop(0)
        return rows

def gen_zip(country="US"):
    if country == 'US':
        return str(randint(11000, 99999))
    elif country == "CA":
        zip = [choice(string.ascii_uppercase) + str(randint(0, 9)) for i in range(3)]
        return "".join(zip)        
    else:
        raise ValueError("[-] Unsupported country value for zip generation")


def quick_mod(json):
    dto = datetime.now()
    mod_datetime = "{}{}{}_{}{}".format(dto.year, dto.month, dto.day, dto.hour, dto.minute)
    new_street = "Modified: {}".format(mod_datetime)

    if isinstance(json, list):
        for record in json:
            #update the street name and full addr to include date
            currentStreet = record.get("Street_Name")
            record['Street_Name'] = new_street
            record['Address'] = record.get('Address').replace(
                                         currentStreet, new_street)
     
            #record['Sale_Price'] = record.get('Sale_Price') + 5000
            record['TransactionId'] = "1{}".format(randint(100000, 999999))
                                                 
    return json

def trx_basic_mod(data, agent_list, street_name):
    record = data
    del data
    dto = datetime.now()
    mod_datetime = "{}{}{}_{}{}".format(dto.year, dto.month, dto.day, dto.hour, dto.minute)
    new_street = "{} {}".format(street_name, mod_datetime)

    if isinstance(record, dict):
        # store initial values for replacement
        currentstreet = record.get("Street_Name")
        currentnumber = record.get("Street_Number")
        currentstate = record.get("State")
        currentaddress = record.get("Address")
        # update the agent info
        random_row = choice(agent_list)
        record['Agent_ID'] = random_row[0]
        record['Email'] = random_row[1]
        record['Office_Name'] = random_row[2]
        record['State'] = random_row[3]
        # update the street name and full addr to include date
        record['Street_Number'] = randint(1, 9999999)
        record['Street_Name'] = new_street
        new_address = "{num} {name} {unit} {city}, {state} {zipcode}".format(
                num = record.get("Street_Number", 0000),
                name = record.get("Street_Name", "i have no name"),
                unit = "#101",
                city = record.get("City", "nowhere"),
                state = record.get("State", None),
                zipcode = record.get("Zip", 12345)
                )
        record['Address'] = new_address

        # skyslope transaction id
        record['TransactionId'] = "1{}".format(randint(10000000, 99999999))
    return record


def perf_mod(json, agent_list, street_name):
    dto = datetime.now()
    mod_datetime = "{}{}{}_{}{}".format(dto.year, dto.month, dto.day, dto.hour, dto.minute)
    new_street = "{} {}".format(street_name, mod_datetime)

    if isinstance(json, list):
        for record in json:
            # store initial values for replacement
            currentstreet = record.get("street_name")
            currentnumber = record.get("street_number")
            currentstate = record.get("state")
            currentaddress = record.get("address")
            # update the agent info
            random_row = choice(agent_list)
            record['agent_id'] = random_row[0]
            record['email'] = random_row[1]
            record['office_name'] = random_row[2]
            record['state'] = random_row[3]
            # update the street name and full addr to include date
            record['street_number'] = randint(1, 9999999)
            record['street_name'] = new_street
            new_address = "{num} {name} {unit} {city}, {state} {zipcode}".format(
                    num = record.get("street_number", 0000),
                    name = record.get("street_name", "i have no name"),
                    unit = "#101",
                    city = record.get("city", "nowhere"),
                    state = record.get("state", none),
                    zipcode = record.get("zip", 12345)
                    )
            record['address'] = new_address

            # skyslope transaction id
            record['transactionid'] = "1{}".format(randint(10000000, 99999999))
                                                 
    return json


def write_to_json_file(filename, jsonObj):
    """write to a file whick wil lbe used to maintain the current status
       during when the requested data needs to be modified.
    """
    with open(filename, "w") as json_file:
        json_file.write(json.dumps(jsonObj, indent=4))


if __name__=='__main__':
    jd = [
            {
                "Name": "Bob",
                "Number": "123",
                "Street_Name": "pheak address",
                "Address": "1 pheak address #P1 Sedona, AZ 29688",
                "Sale_Price": 150000
            },
            {
                "Name": "Bob",
                "Number": "123",
                "Street_Name": "pheak address",
                "Address": "2 pheak address #P1 Sedona, AZ 29688",
                "Sale_Price": 120000
            }
    ]
    print(quick_mod(jd))
