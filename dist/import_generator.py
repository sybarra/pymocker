import json
import csv
import argparse
import requests
from requests.auth import HTTPBasicAuth
from pprint import pprint

class TrxImportGenerator(object):

    def __init__(self, infile="", start_num=int(), end_num=int(), street_name="Generated Import",
                 email=None):
        self.infile = infile
        self.start_num = start_num
        self.end_num = end_num
        self.street_name = street_name
        self.readfile()
        if email:
            self.email = list(self.email)
        else:
            self.email = None

    def readfile(self):
        with open(self.infile, 'r') as jfile:
            data = jfile.read()
        self.json_data = json.loads(data)[0]

    def writefile(self, import_data, out_file='import.json'):
        with open(out_file, 'w+') as f:
            json.dump(import_data, f, indent=4)

    def update_transaction_id(self, value):
        """Set the value for TransactionId"""
        self.json_data['TransactionId'] = int(value)


    def update_street_number(self, value):
        """Updates the street number and full Address"""
        pieces = self.json_data['Address'].split()
        pieces[0] = str(value)
        self.json_data['Address'] = " ".join(pieces)
        self.json_data['Street_Number'] = value

    def update_street_name(self, unit_num="#1"):
        """Updates the street name and the street name in the Full Address field"""
        st_name = self.json_data['Street_Name']
        self.json_data['Address'] = self.json_data['Address'].replace(
            st_name, self.street_name)
        self.json_data['Address'] = self.json_data['Address'].replace(
            '#1', unit_num)
        self.json_data['Street_Name'] = self.street_name

    def update_agent_name(self, name):
        """Updating the agent name.
        param name: (string) space delimited name, <firstname lastname>
        """
        name = name.split()
        self.json_data['First_Name'] = name[0].strip()
        self.json_data['Last_Name'] = name[-1].strip()

    def update_agent_email(self, email):
        """Updating the agent email."""
        self.json_data['Email'] = email

    def update_agent(self, agent_id, email):
        """Updating the agents email and name using single input of email.
        param value: (string) email
        """
        #parse the email to name
        name = " ".join(email.split("@")[0].split("."))
        self.update_agent_name(name)
        self.update_agent_email(email)
        self.json_data['Agent_ID'] = agent_id


class ApiCli(object):

    def __init__(self, env="accp", auth_token=None, username=None, headers={}, payload={},
                query=None):

        # Environment data sets
        self.environments = {
            "uat": {"url": "https://expenterprise-uat.mendixcloud.com/rest", "token": "{AES}4brlyHjDir3I/9y6oociCg==;YwEkafnwKqOMBm2HGAwfDO7oRLAxeQzg3Z30GzSDLMU356szVvrVtjj3eNUIPS8e"},
            "accp": {"url": "https://expenterprise-accp.mendixcloud.com/rest", "token": "{AES}EGexx1r6mONf2SMKuVbx7w==;ur0+JXHzufQy3PiNI2sGSRsGtGb9Cdj0jSOCyGRPf4Llgvo503DoEfm7bJdv6B25"},
            "test": {"url": "https://expenterprise-test.mendixcloud.com/rest", "token": ""},
            "performance": {"url": "https://expenterprise-performance.mendixcloud.com/rest", "token": "{AES}tvaf85PxbeicYowzXq7YJg==;JfcKwEtPZ4TKRaGHnKGu+G1XgsnSFLew++sFAQ/fgwyNINgIeBdbYmclGE8bZ/yL"}
        }

        #self.app_url = "https://expenterprise-{}.mendixcloud.com{}".format(self.env, "/rest")
        self.app_url = None
        self.endpoint = "/agents"
        self.auth_token = auth_token
        self.username= username
        self.password = None
        self.headers = headers
        self.payload = payload
        self.query = {"jql": query}
        self.json_response = None
        self.env = env


    @property
    def env(self):
        return self._env

    @env.setter
    def env(self, env):
        if env not in self.environments.keys():
            raise ValueError("[-] Allowable values for environment: {}".format(self.environments.keys()))

        self._env = env
        self.app_url = self.environments[env].get("url")
        self.auth_token = self.environments[env].get("token")

    def check_response(self, response):
        """writes error to terminal if response status is not 200
        response (obj): requests response object
        """
        if response.status_code != 200:
            print("[-] Error: Status Code {code} : {txt}".format(
                code = response.status_code,
                txt = response.text
                )
            )
            self.json_response = response.text
        else:
            print("Request: [+] {code} : {url}".format(
                code = response.status_code,
                url = response.url
                )
            )
            self.json_response = response.json()

    def print_to_term(self):
        print(json.dumps(self.json_response, sort_keys=True, indent=4))

    def get(self):
        request_url = "{}/{}".format(self.app_url, self.endpoint)
        if self.auth_token:
            self.headers['Authorization'] = self.auth_token
            response = requests.request("GET", request_url, data=self.payload,
                                       headers=self.headers, params=self.query)
        else:
            response = requests.request("GET", request_url, data=self.payload,
                                     headers=self.headers, params=self.query,
                                     auth=HTTPBasicAuth(self.username, self.password))
        self.check_response(response)
        return response

    def search(self, to_term=False):
        """returns a the requests response object
        to_term (bool): If True, print to terminal
        """
        url = "{}/search".format(self.base_url)
        response = requests.request("GET", url, data=self.payload,
                    headers=self.headers, params=self.query)
        self.check_response(response)

        if to_term: self.print_to_term()

        return response

    def get_issue(self, issue, to_term=False):
        url = "{0}/issue/{1}".format(self.base_url, issue)
        response = requests.request("GET", url, headers=self.headers)
        self.check_response(response)

        if to_term: self.print_to_term()

        return response

    def total_issues(self):
        return self.json_response['total']


def argparser():
    parser = argparse.ArgumentParser(description="")
    parser.add_argument('--email', type=str, nargs='+', default=None,
        help="Space delimited list of agents email address \
              (default: 'appteam2@exprealty.com')")

    parser.add_argument('--infile', type=str,
        default='./SingleBuyer_TraditionalSale.json',
        help="JSON file data with template data \
              (default: './SingleBuyer_TraditionalSale.json')")

    parser.add_argument('--transactions', type=int, default=1,
        help="The total number of transactions to be created (default: 1)")

    parser.add_argument('--agents', type=int, default=1,
                        help="The number of agents across which to distribute \
                        the transactions (default: 1)")

    parser.add_argument('--startId', type=int, default=9900,
        help="Agent ID to start with (default: 9900)")

    parser.add_argument('--streetName', type=str, default="Quality Ave",
        help="Custom Street Name (defualt: 'Quality Ave')")

    parser.add_argument('--unit', type=str, default="#1",
        help="The unit number to use in the address (default: '#1')")

    parser.add_argument('--env', type=str, default="accp",
        help="Evironment to get agent data from. \
              Valid values: accp, test, performance, uat. (default: 'accp')")

    parser.add_argument('--skyslopeId', type=int, default=1000100,
        help="Skyslope transaction ID. Each new transaction generated will \
              get +=1. (default: '1000100')")

    return parser.parse_args()


def get_agent_email_list(env, from_id, count=50):
    """ Return a tuple of agent id and email address.
    from_id: (int) the agent id to start with.
                 (default: 9816)
    count: (int) the number of agent emails to be returned.
               (default: 50)
    """
    #Sending basic get request
    cli = ApiCli(env=env)
    #TODO: need to move the usename passsword config to the ApiCli env dict
    cli.username = "testClient"
    cli.passoword = "Re*16beer30?"

    cli.query = {"fromId":from_id, "lifecycleStatus": "Active", "limit": count}
    response = cli.get()
    #cli.print_to_term()
    for record in cli.json_response:
        yield (record["source_system_member_key"], record["member_email"])


if __name__=='__main__':

    #Read cli args
    args = argparser()

    #Inputs
    total_transactions = args.transactions
    total_agents = args.agents
    unit_number = args.unit
    street_name = args.streetName
    start_agent_id = args.startId
    environment = args.env
    if (total_transactions % total_agents) == 0:
        transactions_per_agent = int(total_transactions / total_agents)
    else:
        raise ValueError(
            "Total transactions must be evenly divisible by total agents")


    #Get generating
    generator = TrxImportGenerator(infile=args.infile)
    generator.start_num = 1
    generator.street_name = street_name
    generator.end_num = generator.start_num + transactions_per_agent

    #Create a csv file. This is used for test data definition by JMeter.
    csv_file = open('test_data.csv', 'w+')
    csvwr = csv.writer(csv_file)
    headers = ["Agent ID", "Address"]
    csvwr.writerow(headers)


    import_data = []
    if not generator.email:
        generator.email = get_agent_email_list(environment, start_agent_id,
                                                count=total_agents)

    skyslope_id = args.skyslopeId
    for index, agent_info in enumerate(generator.email):
        for i in range(generator.start_num, generator.end_num):
            generator.readfile()
            generator.update_agent(agent_info[0], agent_info[1])
            generator.update_street_number(i)
            generator.update_street_name(unit_number)
            generator.update_transaction_id(skyslope_id)
            skyslope_id += 1
            import_data.append(generator.json_data)

            #Write test data to csv for JMeter import
            csvrow = [agent_info[0], generator.json_data['Address']]
            csvwr.writerow(csvrow)

        print("Writing file for Agent '{} | {}' with Address like '{}'".format(
           import_data[index]['Agent_ID'],
           import_data[index]['Email'],
           import_data[index]['Address']))

    generator.writefile(import_data)
    csv_file.close()


