#!/bin/env python
from flask import Flask, json, request
import moder as qm

# quick test example JSON data
companies = [
        {
            "id": 1,
            "name": "Company One"
        }, 
        {
            "id": 2,
            "name": "Company Two"
        }
]


api = Flask(__name__)
template_dir = "../templates"

# Example "companies" end point
@api.route('/companies', methods=['GET'])
def get_companies():
    return json.dumps(companies)


# Endpoint used for Skyslope / Trx queue testing
@api.route('/masterdatareport', methods=['POST'])
def get_new_skyslope():
    response_file = "response.json"
    
    #Read the JSON file into a object python can understand
    with open(response_file, "r") as infile:
        data = json.load(infile)

    #create json dump to return from current data
    #json_data = json.dumps(data, indent=4)

    #modify the date for the rest call
    #qm.write_to_json_file(response_file, qm.quick_mod(data))

    #Send the data found in the file as the response body
    return json.dumps(data, indent=4)


@api.route('/updatemdr', methods=['POST'])
def get_updates_skyslope():
    """
    Endpoint used for Skyslope / Trx queue testing but this endpoint will
    update the following field and write a new response.json file each time
    it is called. (Street_Name, Address, Sale_Price). 
    """
    response_file = "response.json"
    
    #Read the JSON file into a object python can understand
    with open(response_file, "r") as infile:
        data = json.load(infile)

    #create json dump to return from current data
    json_data = json.dumps(data, indent=4)

    #modify the date for the rest call
    qm.write_to_json_file(response_file, qm.quick_mod(data))

    #Send the data found in the file as the response body
    return json_data

@api.route('/trx-skyslope', methods=['POST'])
def get_single_transaction():
    """
    """
    country = request.args.get("country", "US")
    street_name = request.args.get("streetname", "TRX Perf Test St.")
    count = int(request.args.get("count", 1))
    response_file = "./templates/{}".format(
            request.args.get("template", "traditional_sale_response.json"))

    return generate_skyslope_response_and_log(response_file, count, country, street_name)


@api.route('/trx-skyslope-ca', methods=['POST'])
def get_ca_single_transaction():
    """
    """
    country = request.args.get("country", "CA")
    street_name = request.args.get("streetname", "CA TRX Perf Test St.")
    count = int(request.args.get("count", 1))
    response_file = "./templates/{}".format(
            request.args.get("template", "traditional_sale_response.json"))

    return generate_skyslope_response_and_log(response_file, count, country, street_name)


def generate_skyslope_response_and_log(response_file, count, country, street_name):
    '''
    param: response_file (str) Name of the file where the next
           repsonse will be written
    param: country (list) one or more countries, that for which
           seed agents will be used.
    '''
    #modify the date for the rest call
    # TODO: support countries should be moved to a config file
    us_country = ["us", "united states"]
    canada_country = ["ca", "canada"]
    supported_countries = [us_country, canada_country]
    # Get the country code based on comparison of country to country lists
    cc = [x[0].upper() for x in supported_countries if country.lower() in x][0]

    if country.lower().replace(" ", "_") in us_country:
        agentfile = "./dist/{}_agent_test_data.csv".format(cc)
        agent_list = qm.list_from_csv(agentfile)

    elif country.lower().replace(" ", "_") in canada_country:
        agentfile = "./dist/{}_agent_test_data.csv".format(cc)
        agent_list = qm.list_from_csv(agentfile)

    else:
        raise ValueError("[-] Specified country is not supported")
                
    response_list = []
    filename = response_file.split("/")[2]
    data_log = "{}.csv".format(filename.split(".")[0])
    print("LOG FILE NAME: " + data_log)
    for i in range(count):
        #Read the JSON file into a object python can understand
        with open(response_file, "r") as infile:
            data = json.load(infile)
        data['Zip'] = qm.gen_zip(cc)
        resp = qm.trx_basic_mod(data, agent_list, street_name)   
        print("OLD: {}, NEW: {}".format(data['Zip'], resp['Zip']))
        response_list.append(resp)

        # Log the transaction's address for use as test data
        if qm.check_data_log_file(data_log):
            qm.add_to_data_log(data_log, resp)

    return json.dumps(response_list, indent=4)


@api.route('/trx-count-100', methods=['POST'])
def get_hundred_transactions():
    """
    """
    country = request.args.get("country", "US")
    street_name = request.args.get("streetname", "US TRX Perf Test St.")
    response_file = "response_100.json"
    return generate_skyslope_response_and_log(response_file, country, street_name)


@api.route('/trx-count-500', methods=['POST'])
def get_five_hundred_transactions():
    """
    """
    country = request.args.get("country", "US")
    street_name = request.args.get("streetname", "US TRX Perf Test St.")
    response_file = "response_500.json"
    return generate_skyslope_response_and_log(response_file, country, street_name)


@api.route('/trx-count-1000', methods=['POST'])
def get_thousand_transactions():
    """
    """
    response_file = "response_1000.json"
    
    #Read the JSON file into a object python can understand
    with open(response_file, "r") as infile:
        data = json.load(infile)

    #create json dump to return from current data
    json_data = json.dumps(data, indent=4)

    #modify the date for the rest call
    qm.write_to_json_file(response_file, qm.perf_mod(data))

    #Send the data found in the file as the response body
    return json_data

@api.route('/perf', methods=['POST'])
def get_updates_perf_skyslope():
    """
    """
    response_file = "response.json"
    
    #Read the JSON file into a object python can understand
    with open(response_file, "r") as infile:
        data = json.load(infile)

    #create json dump to return from current data
    json_data = json.dumps(data, indent=4)

    #modify the date for the rest call
    qm.write_to_json_file(response_file, qm.perf_mod(data))

    #Send the data found in the file as the response body
    return json_data


if __name__=='__main__':
    api.run(host='0.0.0.0')
